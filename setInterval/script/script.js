// =====================THEORY=========================
/*
1. В чому відмінність між setInterval та setTimeout?
setTimeout це колбек функція, яка задає затримку виконання тої чи іншоої події на визначений час. В той час як setInterval задає ітерацію події на також визначений час.

2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
Ні setTimeout визначає точний час оскільки, наступна дія буде виконуватися відрязу по закінчення попередньої дії, в той час як setInterval враховує час на виконання самої функції який взятий з основного часу ітерації., що в свою чергу відіграє на неточність.

3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
З допомогою функції clearTimeout  перериває таймер і встановлена функція вже не визивається. Для інтервалів можна використовувати функцію clearInterval, яка також призупиняє роботу інтервалу.
Це нам необхідно коли нам не потрібна на даний момент робота таймера чи інтервала.
*/

// =====================PRACTICE=======================

// 1.

const button = document.querySelector('.btn');
const text = document.querySelector('.text');

button.addEventListener('click', function(event) {
	if (event.target){
		setTimeout(() => {
			text.classList.toggle('hide')
		}, 3000);
	}
})

// 2.

const countDown = document.querySelector('.count');
let time = 10;

setInterval(updateCountDown, 1000);


function updateCountDown(){
	countDown.innerHTML = `${time}`;
	time--;
	if(time <= -1){
		countDown.innerHTML = "Зворотній відлік закінчено!"
	}
}

